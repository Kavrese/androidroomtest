package com.example.androidroomtestproject1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var noteViewModel: NoteViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fab.setOnClickListener {
            val intent = Intent(this, NoteActivity::class.java)
            startActivityForResult(intent, NEW_NOTE_CODE)
        }

        val noteAdapter = AdapterRec(object: onDeleteClickListener{
            override fun onDeleteClickListener(note: Note) {
                noteViewModel.delete(note)
            }
        })

        rec.adapter = noteAdapter
        rec.layoutManager = LinearLayoutManager(this)

        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel::class.java)

        noteViewModel.allNotes.observe(this, Observer { notes ->
            notes.let {
                noteAdapter.setNotes(notes)
            }
        })
    }



    companion object {
        private val NEW_NOTE_CODE = 1
        val UPDATE_NOTE_CODE = 2
    }
}