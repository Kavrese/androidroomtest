package com.example.androidroomtestproject1

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
@Database(entities = [Note::class], version = 1)
abstract class NoteDatabase: RoomDatabase() {

    abstract fun noteDao(): NoteDao

    companion object{

        @Volatile
        private var noteRoomInstance: NoteDatabase? = null

        internal fun getDatabase(context: Context): NoteDatabase?{
            if (noteRoomInstance == null){
                synchronized(NoteDatabase::class.java){
                    if (noteRoomInstance == null) {
                        noteRoomInstance = Room.databaseBuilder(
                            context.applicationContext,
                            NoteDatabase::class.java, "note_db"
                        ).build()
                    }
                }
            }
            return noteRoomInstance
        }
    }
}