package com.example.androidroomtestproject1

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdapterRec(deleteClickListener: onDeleteClickListener): RecyclerView.Adapter<AdapterRec.ViewHolder>() {

    var noteList = mutableListOf<Note>()

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val name = itemView.findViewById<TextView>(R.id.name)
        val delete = itemView.findViewById<ImageView>(R.id.delete)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        TODO("Not yet implemented")
    }

    override fun getItemCount(): Int = noteList.size

    fun setNotes(notes: List<Note>) {
        noteList = notes.toMutableList()
        notifyDataSetChanged()
    }
}

interface onDeleteClickListener{
    fun onDeleteClickListener(note: Note)
}