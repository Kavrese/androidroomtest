package com.example.androidroomtestproject1

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class NoteViewModel(application: Application): AndroidViewModel(application) {

    private val noteDao: NoteDao
    internal val allNotes: LiveData<List<Note>>

    init {
        val noteBD = NoteDatabase.getDatabase(application)
        noteDao = noteBD!!.noteDao()
        allNotes = noteDao.allNote
    }

    fun update(note: Note){
        UpdateAsyncTask(noteDao).execute(note)
    }

    fun delete(note: Note){
        DeleteAsyncTask(noteDao).execute(note)
    }

    fun insert(note: Note){
        InsertAsyncTask(noteDao).execute(note)
    }

    companion object {
        private class InsertAsyncTask(private val noteDao: NoteDao): AsyncTask<Note, Void, Void>(){
            override fun doInBackground(vararg notes: Note): Void? {
                noteDao.insert(notes[0])
                return null
            }
        }

        private class UpdateAsyncTask(private val noteDao: NoteDao): AsyncTask<Note, Void, Void>(){
            override fun doInBackground(vararg notes: Note): Void? {
                noteDao.update(notes[0])
                return null
            }
        }

        private class DeleteAsyncTask(private val noteDao: NoteDao): AsyncTask<Note, Void, Void>(){
            override fun doInBackground(vararg notes: Note): Void? {
                noteDao.delete(notes[0])
                return null
            }
        }
    }
}